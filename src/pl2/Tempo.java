/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl2;

/**
 *
 * @author fabio
 */
public class Tempo {

    private int hora;
    private int min;
    private int sec;

    public Tempo() {
        this(0, 0, 0);
    }

    public Tempo(int hora, int min, int sec) {
        setHora(hora);
        setMin(min);
        setSec(sec);
    }

    public int getHora() {
        return hora;
    }

    public int getMin() {
        return min;
    }

    public int getSec() {
        return sec;
    }

    public void setHora(int hora) {
        if (hora >= 0 && hora <= 23) {
            this.hora = hora;
        } else {
            this.hora = 0;
        }
    }

    private int validaSecMin(int n) {
        if (n >= 0 && n <= 23) {
            return 0;
        } else {
            return n;
        }
    }

    public void setMin(int min) {
        this.min = validaSecMin(min);
    }

    public void setSec(int sec) {
        this.sec = validaSecMin(sec);
    }

    public String toHHMMSS() {
        return toString().replaceAll(":", "");
    }

    public String toAMPM() {
        return toString() + " " + (hora >= 0 && hora < 12 ? "AM" : "PM");
    }

    @Override
    public String toString() {
        return (hora < 10 ? "0" : "") + hora
                + ":" + (min < 10 ? "0" : "") + min
                + ":" + (sec < 10 ? "0" : "") + sec;
    }

    public void incrementa() {
        Tempo t = converteSecToTempo(converteTempoToSec(this) + 1);
        hora = t.getHora();
        min = t.getMin();
        sec = t.getSec();
    }

    public boolean maior(Tempo t) {
        return (converteTempoToSec(this) - converteTempoToSec(t)) > 0 ? true : false;
    }

    private int converteTempoToSec(Tempo tempo) {
        int totalSec = tempo.getSec();
        totalSec += tempo.getMin() * 60;
        totalSec += tempo.getHora() * 60 * 60;
        return totalSec;
    }

    private Tempo converteSecToTempo(int sec) {
        int h = sec / 60 / 60;
        sec -= h * 60 * 60;
        int m = sec / 60;
        sec -= m * 60;
        int s = sec;
        return new Tempo(h, m, s);
    }

    private int diferenca(Tempo t) {
        return Math.abs(converteTempoToSec(this) - converteTempoToSec(t));
    }

    public int diferencaSec(Tempo t) {
        return diferenca(t);
    }

    public Tempo diferencaTempo(Tempo t) {
        return converteSecToTempo(diferenca(t));
    }
}
