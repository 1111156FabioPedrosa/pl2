/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl2;

import java.util.Calendar;

/**
 *
 * @author fabio
 */
public class TestData {

    public static void main(String[] args) {
        Calendar hoje = Calendar.getInstance();
        int ano = hoje.get(Calendar.YEAR);
        int mes = hoje.get(Calendar.MONTH) + 1; // Janeiro é 0
        int dia = hoje.get(Calendar.DAY_OF_MONTH);
        
        //a
        Data data1 = new Data(ano, mes, dia);
        //b
        System.out.println(data1);
        //c
        Data data2 = new Data(1974, 4, 25);
        //d
        System.out.println(data2.toAnoMesDiaString());
        //e
        if (data1.maior(data2)) {
            System.out.println("Mais recente: " + data1);
        }
        else {
            System.out.println("Mais recente: " + data2);
        }
        //f
        System.out.println("Diferença de dias: " + data1.diferenca(data2));
        //g
        System.out.println("Dia da semanado 25 de Abril de 1974: " + data2.diaDaSemana());
        //h
        if (data2.anoBissexto(1974)) {
            System.out.println("1974 é bissexto.");
        }
        else {
            System.out.println("1974 não é bissexto.");
        }
        
        if (Data.anoBissexto(1974)) {
            System.out.println("1974 é bissexto.");
        }
        else {
            System.out.println("1974 não é bissexto.");
        }
    }
}
