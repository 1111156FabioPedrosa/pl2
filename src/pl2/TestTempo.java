/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl2;

/**
 *
 * @author fabio
 */
public class TestTempo {
    public static void main(String[] args) {
        Tempo t1 = new Tempo(2, 45, 34);
        System.out.println(t1);
        
        Tempo t2 = new Tempo();
        System.out.println(t2);
        t2.setHora(-5);
        t2.setMin(75);
        t2.setSec(76);
        System.out.println(t2);
        t2.setHora(9);
        t2.setMin(59);
        t2.setSec(45);
        System.out.println(t2);
        
        System.out.println(t2.toHHMMSS());
        System.out.println(t2.toAMPM());
        Tempo t3 = new Tempo(12, 00, 00);
        System.out.println(t3.toAMPM());
        
        t2.incrementa();
        System.out.println(t2);
        t2.setSec(59);
        t2.setMin(59);
        t2.setHora(23);
        System.out.println(t2.toHHMMSS());
        t2.incrementa();
        System.out.println(t2.toAMPM());
        
        System.out.println(t1.maior(t3));
        
        System.out.println(t1.diferencaSec(t2));
        System.out.println(t1.diferencaTempo(t2));
    }
}
